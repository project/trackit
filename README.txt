Trackit Module
---------------------------------------------------
This module provides tracking functionality for the JW Flash player. 
You need to set the track url on the player to use this module.

If you use SWFobject, your code might look like:

<div id="video_flash_container">
  <?php 
  $params = array(
    'type' => 'playlist',
    'width' => 320, 
    'height' => 260,
    'allowfullscreen' => 'true',
  );
  
  $vars = array(
    'file' => $filepath,
    'shuffle' => 'false',
    'repeat' => 'list',
    'transition' => 'fade',
    'callback' => 'http://www.mysite.com/trackit',
  );
  print theme("swfobject_api", base_path() . path_to_theme() . '/flash/mediaplayer.swf', $params, $vars); ?>
</div><!-- /video_flash_container -->

If you don't use swfobject, you just need to add 

&callback=http://www.mysite.com/trackit

to the flashvars